# eslint 规则配置

# 安装
`yarn add https://gitlab.com/devops-rb/rb-eslint-config.git --dev`

# 使用

* vue 项目

`.eslintrc.js` 添加
```
extends: [
  'rb/vue',
],
```

* react 项目

`.eslintrc` 添加
```
"extends": [
  "rb/react',
],
```

* 小程序项目

`.eslintrc` 添加
```
"extends": [
  "rb/weapp',
],
```

* 其他

`.eslintrc` 添加
```
"extends": [
  "rb',
],
```
